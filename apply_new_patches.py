#!/usr/bin/python

# File: apply_new_patches.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import sys
import traceback
from wine_repository import *
from config import *
from mail_patches import *
from single_instance import *

LOCAL_TESTING = False

# check for single instance
this_app = single_instance();

if this_app.already_running():
    print "Already running"
    sys.exit(1)

this_app.run_once()

try:
    # check local testing mode
    if (len(sys.argv) > 1 and len(sys.argv) < 4) or (len(sys.argv) > 1 and sys.argv[1] == "--help") :
        print("Usage: apply_new_patches.py (automatic mode)")
        print("       apply_new_patches.py --test <patch filename> <repository name(s)>   # testing mode");
        sys.exit(0)
    if len(sys.argv) == 4 and sys.argv[1] == "--test":
        print "------- LOCAL TESTING MODE ----------"
        LOCAL_TESTING = True
        local_patch_name = os.path.abspath(sys.argv[2])
        print "Patch to test: ", local_patch_name
        local_patch_tags = sys.argv[3]
        print "Tags: ", local_patch_tags

    # Create repositories
    repos = []
    for repo in repositories:
        repos.append(wine_repository(repo))

    for repo in repos:
        repo.update()

    if not LOCAL_TESTING:
        patches = mail_patches(repos)
        patches.connect(IMAP_SERVER, IMAP_LOGIN, IMAP_PASS)
        print "Returns number of new messages" , patches.new_messages()

        # Process patches and merge-tasks
        while True:
            patch = patches.get_next_task()
            if not patch:
                break
            patch.run()
        patches.disconnect()
    else:
        # LOCAL_TESTING
        tmp_patch = wine_patch(local_patch_name, local_patch_tags, repos, TESTER_MAIL);
        tmp_patch.enable_testing_mode()
        print("Do not realized yet.");
        sys.exit(1);
        #patches.append(tmp_patch)

    # print "All done correctly"
    this_app.close()
except KeyboardInterrupt:
    print("interrupted from keyboard")
    this_app.close()

except SystemExit, e:
    this_app.close()
    sys.exit(e)

except:
    this_app.close()
    print ("Unhandled exception:\n" + traceback.format_exc())
    #logger.error("Unhandled exception:\n" + traceback.format_exc())
