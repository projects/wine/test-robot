#!/usr/bin/python

# File: config.py.
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

""" Example of configuration file. Please, modify it and save as config.py """

TMP_DIR = "/srv/builder/tmp/wine-tests"
LOCK_FILE = "lock" # will be created in the TMP_DIR

# Email configuration
SMTP_SERVER = 'mail.etersoft.ru'
SMTP_SERVER_PORT = 587
IMAP_SERVER = 'mail.etersoft.ru'
IMAP_LOGIN = 'wine-patches-test@office.etersoft.ru'
IMAP_PASS = 'patchtestetersoft'
MAIL_FROM = "Test robot <wine-patches-test@office.etersoft.ru>"
MAILING_LIST = "wine-devel@lists.etersoft.ru"

ADMIN_MAIL  = "Vitaly Perov <vitperov@etersoft.ru>"
TESTER_MAIL = ADMIN_MAIL
MAIL_CC = ADMIN_MAIL

# Repositories configuration
BASE_PATH = "~/Projects/"

eterwine = {
    "directory":     "eterwine-test",
    "repository":    "git.eter:/projects/eterwine.git",
    "master_branch": "master",
    "tag":           "ETERWINE",
    "merge_with": {
        "repository": "git.eter:/projects/wine/wine-pure.git",
        "branch":     "master"
        },
    "specfile": "etersoft/wine.spec"
    }

eterhack = {
    "directory":     "eterhack-test",
    "repository":    "git.office:/projects/eterhack.git",
    "master_branch": "eterhack",
    "tag":           "ETERHACK",
    "merge_with": {
        "repository": "git.eter:/projects/eterwine.git",
        "branch":     "master"
        },
    "specfile": "etersoft/wine-etersoft.spec"
    }

eter_1_0_12 = {
    "directory":     "1.0.12-test",
    "repository":    "git.office:/projects/eterhack.git",
    "master_branch": "eter-1.0.12",
    "tag":           "ETER-1.0.12"
    }

school = {
    "directory":     "1.7.0-test",
    "repository":    "git.office:/projects/eterhack.git",
    "master_branch": "eter-1.7.0",
    "tag":           "SCHOOL"
    }

repositories = [eterwine, eterhack, eter_1_0_12, school]

developers_whitelist = [
    "vitperov@etersoft.ru",
    "lav@etersoft.ru",
    "amorozov@etersoft.ru",
    "kondratyuk@etersoft.ru",
    "stas@etersoft.ru"
    ]

# Project scripts (To lock the repositories for all scripts)
# NOTE: you should use output of get_proc_name_by_pid() function
project_scripts = [
    'apply_new_patch',
    'maintain_repo.p'
    ]
