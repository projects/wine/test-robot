#!/usr/bin/python

# File: git_patch.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import os
import re
from logger import *
from config import *

class git_patch(logger):

    def __init__(self, filename, tags, repos, author):
        logger.__init__(self)
        self._init_mail_report(MAIL_FROM)
        self._set_mail_recipient(author)
        self.author = author

        self.screen_debug = True
        self.debug("Initialization. File='" + filename + "'")
        self.filename = filename
        self.repos = repos
        self.tags = tags
        self.testing_mode = False
        self.subject = "(No subject)"

        n_repos = self.find_self_repos()
        if n_repos < 1:
            self.error("The patch doesn't match to any repository")
        self.info = {}

    def find_self_repos(self):
        """ list of repositories where the patch should be applied """
        self.self_repos = []
        found = 0
        for repo in self.repos:
            if repo.name in self.tags:
                self.self_repos.append(repo)
                repo.set_mail_report(self._get_mail_report())
                found += 1
        return found

    def test_apply(self, repo):
        """ try to apply the patch into given repository
        Return value: if applied successfully returns commit informantion,
        if not - returns None """
        self.debug("test_apply call")
        result = repo.apply_commit(self.filename)
        if not result:
            return None
        info = repo.last_commit_info()
        repo.revert_last_commit()
        return info

    def is_correct(self):
        """ check author, whitespaces and bug number """
        # check if the sender is in whitelist
        mail = re.search(ur'[\w\-][\w\-\.]+@[\w\-][\w\-\.]+[a-zA-Z]{1,4}', self.author)
        mail = mail.group()
        if not mail in developers_whitelist:
            self.error("The mail-sender '" + mail + "' is not in whitelist")
            return False
        return True

    def report_applied(self):
        self._send_mail_report("Re: " + self.subject + ". Your patch is applied" )

    def report_partially_applied(self):
        if not self.testing_mode:
            self._enable_mailing_list_reply_()
        self._send_mail_report("Re: " + self.subject + ". Your patch is not correctly applied" )

    def report_rejected(self):
        if not self.testing_mode:
            self._enable_mailing_list_reply_()
        self._send_mail_report("Re: " + self.subject + ". Your patch is rejected"  )

    def remove_patch_file(self):
        self.debug("Removing patch file '" + self.filename + "'")
        if not self.testing_mode:
            os.remove(self.filename)
        else:
            self.debug("Testing mode: Skip removing the patch file")

    def set_subject(self, subject):
        self.subject = subject

    def enable_testing_mode(self):
        self.testing_mode = True
        self.screen_debug = True
