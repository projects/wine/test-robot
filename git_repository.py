#!/usr/bin/python

# File: git_repository.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import traceback
from git import *
from logger import *

class git_repository(Repo, logger):
    BUILD_LOG_FILE = "wine_build.log"
    TEST_GREP = "| grep 'Test failed'"
    #TEST_GREP = ""

    def __init__(self, repo_data):
        Repo.__init__(self, os.path.join(BASE_PATH, repo_data['directory']))
        logger.__init__(self)
        assert self.bare == False
        self.name = repo_data['tag']
        self.masterbranch = repo_data['master_branch']
        self.merge_with = None
        try:
            self.merge_with = repo_data['merge_with']
        except:
            pass
        self.specfile = None
        try:
            self.specfile = repo_data['specfile']
        except:
            pass
        self.screen_debug = True

    def update(self):
        #self.debug("Update repository " + self.name)
        origin = self.remotes.origin
        # remove tmp branch (it it exists after previous crash). Eterbug #6656
        removed = self.silent_remove_tmp_branch()
        if removed:
            self.warning("Temporary branch found. Probably an error has been occurred during last execution")
        try:
            #self.git.execute(["git", "clean", "-d", "-f"])
            self.git.execute(["git", "checkout", "-f"])
            self.git.execute(["git", "pull", "origin", self.masterbranch])
            #self.debug("Updated correctly")
        except GitCommandError:
            self.debug("Some error occured during update. Backtrace:" + traceback.format_exc())
            return False

    def publish(self):
        self.debug("Publish repository " + self.name)
        try:
            self.git.execute(["git", "push", "origin", self.masterbranch])
            self.debug("Published correctly")
            return True
        except GitCommandError:
            self.error("Error occured during the 'git push'. Backtrace:" + traceback.format_exc())
            return False

    def apply_commit(self, filename):
        self.debug("Repository '" + self.name + "'. Applying the commit file " + filename)
        try:
            result = self.git.execute(["git", "am", filename])
        except:
            self.error("Can't apply the patch:" + traceback.format_exc())
            self.debug("aborting...")
            try:
                self.git.execute(["git", "am", "--abort"])
            except:
                self.error("Can't abort the patch applying: " + traceback.format_exc())
                return False
            self.debug("successfully aborted")
            return False
        #self.debug("successfully applied")
        return True

    def revert_last_commit(self):
        self.debug("Repository '" + self.name + "'. Reverting the last commit")
        headcommit = self.head.commit
        previous = headcommit.parents[0]
        self.head.commit = previous
        self.head.reset(index=True, working_tree=True)
        self.debug("successfully reverted")
        return True

    def last_commit_info(self):
        self.debug("Repository '" + self.name + "'. Retrieving the last commit information")
        headcommit = self.head.commit
        commitinfo = {};
        commitinfo['author'] = headcommit.author.name
        commitinfo['email'] = headcommit.author.email
        commitinfo['files'] = headcommit.stats.files
        return commitinfo

    def _checkout_master_(self):
        #switch to the master branch if not in master now
        this_name = self.head.reference.name
        if this_name == self.masterbranch:
            #already in master
            return
        self.git.execute(["git", "checkout", self.masterbranch])

    def use_tmp_branch(self):
        # work in temporary branch
        self.branch_tmp = self.create_head('tmp')
        self.head.reference = self.heads.tmp
        self.head.reset(index=True, working_tree=True)

    def remove_tmp_branch(self):
        # remove tmp branch and all changes in tmp branch
        self._checkout_master_()
        #self.delete_head('tmp')
        try:
            self.git.execute(["git", "branch", "-D", "tmp"])
            return True
        except GitCommandError:
            self.debug("Repository '" + self.name + "': Can't remove temrorary branch")
            return False

    def silent_remove_tmp_branch(self):
        # remove tmp branch and all changes in tmp branch
        self._checkout_master_()
        try:
            self.git.execute(["git", "branch", "-D", "tmp"])
            return True
        except GitCommandError:
            return False

    def merge_tmp_branch(self):
        self.debug("Repository '" + self.name + "'. Merging from temporary branch to the branch '" + self.masterbranch + "'")
        self._checkout_master_()
        result = self.git.execute(["git", "merge", "tmp"])
        #print "merge result=", result
        self.delete_head('tmp')

    def find_remote(self, name):
        for remote in self.remotes:
            if remote.name == name:
                return remote
        return None

    def get_remote(self, name, create_from):
        remote = self.find_remote(name)
        if (remote):
            return remote
        # Don't exists . Create it
        if not create_from:
            return None
        return self.create_remote(name, create_from)

    def get_branch(self, repo, branch):
        for ref in repo.refs:
            name = ref.name
            name = name.split("/")[1]
            print "name = ", name
            if name == branch:
                return ref
        return None

    def merge_with_commit(self, commit_id):
        if not self.merge_with:
            self.error("Cant't merge. Remote repository is not set")
            return False
        merge_repo = self.merge_with['repository']
        merge_branch = self.merge_with['branch']
        self.debug("Repository '" + self.name + "'. Merging with branch '" + merge_branch + "' of '" + merge_repo + "' repository.")
        remote = self.get_remote('merge_from', merge_repo)
        rem_branch = self.get_branch(remote, 'master')
        remote.fetch()
        #remote.pull(rem_branch)
        try:
            self.git.execute(["git", "merge", commit_id])
            self.debug("Merged successfully")
            return None
        except GitCommandError:
            return self.git.diff(numstat=True)

    def merge_with_upstream(self):
        """ merges current repository with a upstream remote repository
            Returns:
                * None if merge was successfull
                * diff string if it was conflicts during the merge;
        """
        if not self.merge_with:
            # Cant't merge. Remote repository is not set
            return False
        merge_repo = self.merge_with['repository']
        merge_branch = self.merge_with['branch']
        self.debug("Repository '" + self.name + "'. Merging with branch '" + merge_branch + "' of '" + merge_repo + "' repository.")
        remote = self.get_remote('merge_from', merge_repo)
        remote.fetch()
        remote.pull(merge_branch)
        if not self.is_dirty():
            self.debug("Merged successfully")
            return None;
        return self.git.diff(numstat=True)

    def resolve_conflicts(self, diff):
        # parse git diff information and try to resolve simple conflicts
        lines = diff.splitlines()
        if len(lines)%2 != 0:
            self.error("Unexpected number of lines (it should be even number)")
            return False
        files = []
        for i in xrange(len(lines)/2):
            line = lines[i*2]
            words = line.split()
            files.append(words[2])
        self.debug("Conflicted files found: " + str(files))
        for curfile in files:
            if self.resolve_conflict_in(curfile):
                continue # resolved
            # try to revert conflicting commits
            # FIXME: unimplemented

            self.fixme("Can't merge conflict in '" + curfile + "'")
            return False # stop processing at first error
        self.debug("All conflicts are resolved. Creating a merge commit")
        # FIXME: we should use git-python class to create a merge commit"
        message = "Merge repository " + self.name + " with upstream"
        try:
            self.git.execute(["git", "commit", "-a", "--message=\"" + message+ "\""])
        except AssertionError:
            self.debug("Some error occured during update. Backtrace:" + traceback.format_exc())
            return False
        return True

    def resolve_conflict_in(self, filename):
        """ Resolve conflicts in particular files """
        # fix conflict in configure
        if filename == 'configure':
            # FIXME: we should check if 'configure' file exists
            self.debug("Auto-resolving configure")
            os.chdir(self.git.working_dir)
            cmd = "autoconf -f"
            os.system(cmd)
            return True
        return False
