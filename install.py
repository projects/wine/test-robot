#!/usr/bin/python

# File: install.py
#
# This is the installatin script. It creates local repositories and project folders acoording to config.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import os
from git import *
from config import *

def create_dir(name):
    if not os.path.isdir(name):
        try:
            os.makedirs(name)
        except OSError:
            print "ERROR: can't create the directory '", dir, "'"
            pass

def clone_repositories(dir, parent, branch, tag):

    create_dir(dir)
    os.chdir(dir)

    print "cloning ", tag, " repository"
    command = "git clone " + parent + " ."
    result = os.system(command)
    print result

    command = "git checkout -b " + branch + " origin/" + branch
    result = os.system(command)
    print result


create_dir(TMP_DIR)

clone_repositories(os.path.join(BASE_PATH, REPO_ETERWINE), PARENT_ETERWINE, BRANCH_ETERWINE, TAG_ETERWINE)
clone_repositories(os.path.join(BASE_PATH, REPO_ETERHACK), PARENT_ETERHACK, BRANCH_ETERHACK, TAG_ETERHACK)
clone_repositories(os.path.join(BASE_PATH, REPO_1_0_12), PARENT_1_0_12, BRANCH_1_0_12, TAG_1_0_12)

print "All done correctly"
