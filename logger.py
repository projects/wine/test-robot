#!/usr/bin/python

# File: logger.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import datetime
import os
from time import gmtime, strftime
from config import *
from send_mail import *

logger_file_handler = None

class file_logger:
    def __init__ (self):
        global logger_file_handler
        if logger_file_handler == None:
            date = strftime('%Y.%m.%d')
            filename = date + ".log"
            filename = os.path.join(TMP_DIR, filename)
            logger_file_handler = file(filename,'a')
            logger_file_handler.write("\n\n-----------------------------------------------\n")

    def log_to_file(self, message):
        global logger_file_handler
        logger_file_handler.write(message + "\n")
        logger_file_handler.flush()

class logger(file_logger):
    def __init__(self):
        file_logger.__init__(self)
        self.screen_debug = False
        self._mail_report = None

    def _write_mail_line_(self, line):
        if self._mail_report:
            self._mail_report.write_mail_report(line + "\n")

    def _enable_mailing_list_reply_(self):
        if self._mail_report:
            self._mail_report.reply_mailing_list = True

    def _print_screen_line_(self,line):
        if self.screen_debug:
            print line

    def _log_message_(self, msg, tag, report_mail=True):
        string = strftime("[%H:%M:%S]")
        string += tag
        string += " " + self.__class__.__name__ + ": " + msg

        self.log_to_file(string)
        if report_mail:
            self._write_mail_line_(string)
        self._print_screen_line_(string)

    def debug(self, msg):
        self._log_message_(msg,"DEBUG", False)

    def error(self, msg):
        self._log_message_(msg,"ERROR")

    def fixme(self, msg):
        self._log_message_(msg,"FIXME")

    def warning(self, msg):
        self._log_message_(msg,"WARNING")

    def report(self, msg):
        self._log_message_(msg,"")

    def set_mail_report(self, mail_report):
        self._mail_report = mail_report

    def unset_mail_report(self):
        self._mail_report = None

    def _init_mail_report(self, sender):
        self._mail_report = report_by_mail(sender)

    def _get_mail_report(self):
        return self._mail_report

    def _send_mail_report(self, subject):
        if self._mail_report:
            self._mail_report.set_subject(subject)
            self._mail_report.send_mail()
            return True
        self.error("Can't send mail. _mail_report variable is unitialized for this class")
        return False

    def _set_mail_recipient(self, name):
        if self._mail_report:
            self._mail_report.to_addr = name
            return True
        self.error("Can't set mail recipient. _mail_report variable is unitialized for this class")
        return False

    def _set_mail_subject(self, subject):
        if self._mail_report:
            self._mail_report.set_subject(subject)
            return True
        self.error("Can't set mail subject. _mail_report variable is unitialized for this class")
        return False

    def _attach_log_(self, filename):
        if self._mail_report:
            self._mail_report.attach_file(filename)
            return True
        self.error("Can't attach the log file. _mail_report variable is unitialized for this class")
        return False
