#!/usr/bin/python

# File: mail_messages.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import email, email.header, imaplib
import email, imaplib
from locale import getpreferredencoding

class mail_messages:
    __cur = -1
    __messages = []

    def __init__(self):
        self.__cur = -1

    def connect(self, server, login, passw):
        self.m = imaplib.IMAP4_SSL(server)
        self.m.login(login, passw)
        self.m.select()
        self.__cur = -1
        typ, data = self.m.search(None, 'ALL')
        self.__messages = data[0].split()

    def disconnect(self):
        self.m.expunge()
        self.m.close()
        self.m.logout()

    def new_messages(self):
        """ Returns number of new messages"""
        ans = self.m.recent()
        num = int(ans[1][0]);
        return num

    def get_next(self):
        self.__cur += 1
        if self.__cur >= len(self.__messages):
            return ''
        typ, data = self.m.fetch(self.__messages[self.__cur], '(RFC822)')
        return data[0][1]

    def delete(self, msg):
        self.m.store(self.__messages[msg.message_num], '+FLAGS', '\\Deleted')

    def get_next_parsed(self):
        unparsed_str = self.get_next()
        if not unparsed_str:
            return None
        unparsed = email.message_from_string(unparsed_str)
        msg = parsed_message()
        msg.sender = unparsed.get("From")
        msg.subject = self._decode_subject(unparsed.get("Subject"))
        msg.body = self.get_first_text_part(unparsed)
        msg.ID = unparsed.get("Message-Id")
        msg.ID = msg.ID[1:20]
        msg.attachments = self.process_attachments(unparsed)
        msg.message_num = self.__cur
        msg.unparsed = unparsed_str
        return msg

    @staticmethod
    def process_attachments(msg):
        attachments = msg.get_payload()
        if type(attachments) == str :
            # no attachments found
            return []
        parsed_attachments = []
        for part in attachments:
            filename = part.get_filename()
            if not filename:
                continue
            parsed = parsed_attachment()
            parsed.filename = filename
            parsed.body = part.get_payload(decode=1)
            parsed_attachments.append(parsed)
        return parsed_attachments

    @staticmethod
    def get_first_text_part(msg):
        maintype = msg.get_content_maintype()
        if maintype == 'multipart':
            for part in msg.get_payload():
                if part.get_content_maintype() == 'text':
                    return part.get_payload()
        elif maintype == 'text':
            return msg.get_payload()

    def _decode_subject(self, enc_subj):
        dec_subj = email.header.decode_header(enc_subj)
        subj = ""
        for text, enc in dec_subj:
            text = enc and text.decode(enc).encode(getpreferredencoding(), 'replace') or text
            subj += subj and " " + text or text
        return subj

class parsed_message:
    sender = ""
    subject = ""
    body = ""
    ID = "";
    attachments = []
    message_num = -1
    unparsed = ""

class parsed_attachment:
    filename = ""
    body = ""
