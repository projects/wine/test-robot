#!/usr/bin/python

# File: mail_patches.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import os
import email, imaplib
from mail_messages import *
from wine_patch import *
from merge_task import *

class mail_patches(mail_messages):
    __patches_buf = []
    __pending_msgs_buf = []

    def __init__(self, repos):
        mail_messages.__init__(self)
        self.repos = repos

    def get_next_task(self):
        """ Return next task (patch or merge-task) """
        # Return patch from previous message
        task = None
        if len(self.__patches_buf):
            task = self.__patches_buf[0]
            self.__patches_buf = self.__patches_buf[1:]
            return task
        # Parse next message
        while not task:
            msg = self.get_next_parsed()
            if not msg:
                return None
            tags, cur, n = self._parse_subject(msg.subject)
            if len(tags) > 0:
                if cur != -1:
                    task = self.process_patch_series(msg, tags, cur, n);
                elif tags[0] == 'MERGE_TASK':
                    task = self.process_merge_task(msg)
                else:
                    task = self.process_patch_msg(msg, tags)
        return task

    def _create_patch(self, msg, tags, attach):
        suffix = attach and ('-' + attach.filename) or '.patch'
        filename = os.path.join(TMP_DIR, msg.ID + suffix)

        patch = wine_patch(filename, tags, self.repos, msg.sender)
        patch.set_subject(msg.subject)

        f = open(filename, "wb")
        f.write(attach and attach.body or msg.unparsed)
        f.close()
        return patch

    def process_patch_msg(self, msg, tags):
        """ Return wine-patch object """
        if len(msg.attachments) < 1:
            patch = self._create_patch(msg, tags, None)
            self.delete(msg)
            return patch

        ret_patch = None
        for attachment in msg.attachments:
            patch = self._create_patch(msg, tags, attachment)
            # return first patch or add next patches to the __patches_buf
            if not ret_patch:
                ret_patch = patch
            else:
                self.__patches_buf.append(patch)
            self.delete(msg)
        return ret_patch

    def _compare_series(self, m1, m2):
        return m1.msg.sender == m2.msg.sender and m1.tags == m2.tags and m1.n == m2.n

    def process_patch_series(self, msg, tags, cur, n):
        new = pending_msg(msg, tags, cur, n)
        self.__pending_msgs_buf.append(new)
        # check that a patch series is complete
        series = []
        for i in xrange(1, n + 1):
            for m in self.__pending_msgs_buf:
                if self._compare_series(new, m) and m.cur == i:
                    series.append(m)
                    break
            if len(series) < i:
                return None
        # remove messages from __pending_msgs_buf
        for m in series:
            for saved in self.__pending_msgs_buf:
                if self._compare_series(m, saved) and m.cur == saved.cur:
                    self.__pending_msgs_buf.remove(saved)
                    break
        # create patches and add to __patches_buf
        ret_patch = None
        for m in series:
            if not len(m.msg.attachments):
                patch = self._create_patch(m.msg, m.tags, None)
            elif len(m.msg.attachments) == 1:
                patch = self._create_patch(m.msg, m.tags, m.msg.attachments[0])
            else:
                # FIXME: report
                ret_patch = None
                break
            if not ret_patch:
                ret_patch = patch
            else:
                self.__patches_buf.append(patch)
        for m in series:
            self.delete(m.msg)
        return ret_patch

    def process_merge_task(self, msg):
        body = msg.body.split()
        repository = body[0]
        repository = repository.upper()
        commits = body[1:]
        # FIXME: here we should check whether it is correct commit IDs
        repo_match = None
        for repo in self.repos:
            if repo.name == repository:
                repo_match = repo
                break
        if repo_match == None:
            # FIXME: report 'repository not found'
            return False
        task = wine_merge_task(repo_match, commits, msg.sender)
        task.set_subject(msg.subject)
        self.delete(msg)
        return task

    def _get_patch_nums(self, str):
        ns = str.split('/')
        if len(ns) != 2:
            return -1, -1
        try:
            cur = int(ns[0])
            n = int(ns[1])
        except ValueError:
            return -1, -1
        if (cur > n) or (cur < 1) or (n < 1):
            return -1, -1
        return cur, n

    def _parse_subject(self, string):
        """ Returns tags and patch numbers for patches in series """
        tags = [];
        cur = n = -1
        tags_str = ''
        string = string.upper()
        while True:
            begin = string.find('[')
            end = string.find(']')
            if (begin < 0) or (end < 0):
                break
            tags_str += ' ' + string[begin+1:end]
            string = string[end+1::]
        for tag in tags_str.split():
            if '/' in tag:
                if cur != -1:
                    return [], -1, -1
                cur, n = self._get_patch_nums(tag)
                if cur == -1:
                    return [], -1, -1
            else:
                tags.append(tag)
        return tags, cur, n

class pending_msg:
    def __init__(self, msg, tags, cur, n):
        self.msg = msg
        self.tags = tags
        self.cur = cur
        self.n = n
