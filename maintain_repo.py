#!/usr/bin/python

# File: maintain_repo.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import sys
import traceback
from wine_repository import *
from config import *
from single_instance import *
from merge_task import *

LOCAL_TESTING = False

# check for single instance
this_app = single_instance();

if this_app.already_running():
    print "Already running"
    sys.exit(0)

this_app.run_once()

try:
    repository = None
    commit_id = ""
    # check local testing mode
    if len(sys.argv) < 1 or len(sys.argv) > 3:
        print("Usage: maintain_repo.py                                  # merge all branches automatically")
        print("       maintain_repo.py [repository name] [commit ID]    # merge given branch with given commit");
        sys.exit(0)
    if len(sys.argv) == 2:
        repository = sys.argv[1].upper()
    if len(sys.argv) == 3:
        repository = sys.argv[1].upper()
        commit_id = [sys.argv[2]]

    # Create repositories
    repos = []
    for repo in repositories:
        repos.append(wine_repository(repo))

    repos_to_update = repos[0:2]

    if repository:
        # merge given branch with given commit
        found = False
        for repo in repos:
            if repo.name == repository:
                found = True
                print "Updating repository ", repository
                repo.update()
                task = wine_merge_task(repo, commit_id, ADMIN_MAIL)
                task.set_subject("Update " + repo.name + " repository")
                result = task.run()
                if result:
                    print "Merged successfully"
                else:
                    print "Error during the merge"
        if not found:
            print "No such repository '", repository, "'"
    else:
        print "Merge all repositories automatically"
        for repo in repos_to_update:
            print "Updating repository ", repo.name
            repo.update()
            task = wine_merge_task(repo, "", ADMIN_MAIL)
            task.set_subject("Update " + repo.name + " repository")
            result = task.run()
            if result:
                print "Merged successfully"
            else:
                print "Error during the merge"

    this_app.close()
except KeyboardInterrupt:
    print("interrupted from keyboard")
    this_app.close()

except SystemExit, e:
    this_app.close()
    sys.exit(e)

except:
    this_app.close()
    print ("Unhandled exception:\n" + traceback.format_exc())
