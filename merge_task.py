#!/usr/bin/python

# File: merge_task.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import traceback
from git import *
from logger import *
from git_repository import *

class git_merge_task(logger):

    def __init__(self, repo, commits, author):
        logger.__init__(self)
        self._init_mail_report(MAIL_FROM)
        self._set_mail_recipient(author)

        repo.set_mail_report(self._get_mail_report())

        self.screen_debug = True
        self.testing_mode = False
        self.subject = "(No subject)"
        self.repo = repo
        self.commits = commits

    def run(self):
        self.fixme("Not implemented. Use git_wine_merge_task class instead.")

    def set_subject(self, subject):
        self.subject = subject

    def report_success(self):
        self._send_mail_report("Merged successfully (" + self.subject + ")" )

    def report_partially_merged(self):
        self._send_mail_report("Partially merged (" + self.subject + ")" )

    def report_merge_failed(self):
        self._send_mail_report("Merge failed (" + self.subject + ")" )


class wine_merge_task(git_merge_task):

    def run(self):
        self.debug("Running merge task")
        merged = False
        if len(self.commits) < 1:
            self.debug("No commit ID specified. Merge with HEAD of upstream repository")
            old_head = self.repo.head.commit
            result = self.repo.merge_and_test(None)
            if not result:
                self.error("Merge with HEAD of upstream is failed")
                self.report_merge_failed()
                return False
            new_head = self.repo.head.commit
            if old_head == new_head:
                self.debug("There wasn't any changes during the merge")
                return True
            self.debug("Successfully merged with HEAD of upstream")
            self.report_success()
            return True

        for commit in self.commits:
            self.debug("Merging with commit " + commit)
            result = self.repo.merge_and_test(commit)
            if not result:
                self.error("Merge with commit " + commit + " failed")
                if merged:
                    self.report_partially_merged()
                else:
                    self.report_merge_failed()
                return False
        self.debug("Successfully merged with all given commits")
        self.report_success()
        return True
