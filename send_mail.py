#!/usr/bin/python

# File: send_mail.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
import smtplib
import os
from config import *
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.message import Message

class report_by_mail():

    def __init__(self, from_addr, to_addr = ""):
        self.from_addr = from_addr
        self.to_addr = to_addr
        self.message = ""
        self.subject = "No subject"
        self.mail = MIMEMultipart()
        self.mail.set_charset('utf-8')
        self.reply_mailing_list = False

    def set_subject(self, subject):
        self.subject = subject

    def write_mail_report(self, str):
        self.message += str

    def attach_file(self, path):
        fp = open(path)
        msg = MIMEBase('application', "octet-stream")
        msg.set_payload(fp.read())
        encoders.encode_base64(msg)
        [directory, filename] = os.path.split(path)
        msg.add_header('Content-Disposition', 'attachment', filename=filename)
        self.mail.attach(msg)

    def send_mail(self):
        self.mail.attach(MIMEText(self.message, 'plain'))
        self.mail['From'] = self.from_addr
        self.mail['To'] = self.to_addr
        self.mail['Subject'] = self.subject
        if self.reply_mailing_list:
            self.mail['Cc'] = MAILING_LIST + ", " + MAIL_CC
        else:
            self.mail['Cc'] = MAIL_CC

        server = smtplib.SMTP()
        server.connect(SMTP_SERVER, SMTP_SERVER_PORT)
        server.sendmail(self.from_addr, [self.to_addr] + [self.mail['Cc']], self.mail.as_string())
        server.quit()
        self.message = ""


