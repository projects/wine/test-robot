#!/usr/bin/python

# File: single_instance.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import os
import sys
import subprocess
from config import *

def get_proc_name_by_pid(pid):
    cmd = ['ps', '-p', pid, '-o', 'comm=']
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
    return output.replace('\n','')

class single_instance:

    def __init__(self):
        self.path = os.path.join(TMP_DIR, LOCK_FILE)
        self.running = False

    def already_running(self):
        if not os.path.exists(self.path):
            return False
        # check if previous process is dead
        if os.name != 'posix':
            # we can check it only in Posix
            return True
        f = open(self.path, "r")
        lock_pid = f.read()
        f.close()
        pid_name = get_proc_name_by_pid(lock_pid)
        if len(pid_name) < 1:
            # process is already dead
            return False
        self_name = get_proc_name_by_pid(str(os.getpid()))
        project_scripts.append(self_name)
        if pid_name in project_scripts:
            # previous process is still alive
            return True
        # process is already dead
        return False

    def run_once(self):
        self.handle = open(self.path, "w")
        if os.name == 'posix':
            self.handle.write(str(os.getpid()))
        self.handle.close()
        self.running = True

    def close(self):
        if self.running:
            # remove file only if this application is halted
            os.remove(self.path)
