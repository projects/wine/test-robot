#!/usr/bin/python

# File: wine_patch.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import os
from git_patch import *

class wine_patch(git_patch):

    def find_tests_to_run(self):
        if len(self.info) < 1:
            self.error("Patch information structure is empty")
            return False
        files = self.info['files']
        self.tests = []

        for file in files:
            pos = file.find("/tests/")
            if pos > 0:
                # this patch provides tests. We assume, that an author already
                # tested the patch.
                self.report("The patch provides tests. It will be applyed without any testing")
                #self.write_mail_report("The patch provides tests. It will be applyed without any testing")
                return True
            path = os.path.split(file)
            filename = path[1]
            pos = filename.find(".c")
            if pos < 0:
                self.debug("Skip tests for file '" + filename + "'")
                continue
            test_name = filename[0:pos]
            dllname = os.path.split(path[0])[1]
            tests_dir = os.path.join(path[0], "tests")
            executable = dllname + "_test.exe.so"
            full_path = os.path.join(tests_dir, executable)
            filename = path[1]
            test = {}
            test['executable'] = full_path
            test['test_name'] = test_name
            self.tests.append(test)

        self.debug("tests to run: " + str(self.tests));
        return True

    def is_failed_tests(self, res_before, res_after):
        """ check if there are failed tests """
        if len(res_before) != len(res_after):
            self.error("Test count doesn't match (before=" + str(len(res_before)) +", after=" + str(len(res_after)) + ")")
            return True;
        for i in range(0, len(res_before), 1):
            if res_before[i] == "":
                if res_after[i] == "":
                    # this error is already reported by run_test
                    continue
                else:
                    self.error("mismatch test results files");
                    continue
            if res_after[i] == "" and res_before != "":
                self.error("mismatch test results files");
                continue
            # find difference
            command = "diff " + str(res_before[i]) + " " + str(res_after[i]) + " >/dev/null"
            self.debug("Diff command:" + command)
            result = os.system(command)
            #print "Result = ", result
            if result > 0:
                self.debug("The patch changes something");
                # send log files:
                self._attach_log_(res_before[i])
                self._attach_log_(res_after[i])
                # Fix or break the tests?
                lines_before = len(open(res_before[i], 'r').readlines())
                lines_after = len(open(res_after[i], 'r').readlines())
                if lines_before <= lines_after:
                    self.error("Tests failed. Files: before='" + res_before[i] + "', after='" + res_after[i] + "'")
                    return True
                else:
                    self.report("Your patch fixes " + str(lines_before - lines_after) + " error(s). Maybe you should send it to winehq.org? Thank you!")
                    return False
            return False

    def apply_with_tests(self):
        """ apply the patch to the repositories specified in tags """
        self.debug("apply_with_tests call")
        if not self.is_correct():
            return False
        if len(self.self_repos) < 1:
            self.error("Nothing to do. Exit")
            self.report_rejected()
            return False
        applied_to = 0
        try:
            for repo in self.self_repos:
                repo.use_tmp_branch()
                info = self.test_apply(repo)
                if info == None:
                    self.error("The patch doesn't apply to the repository '" + repo.name + "'")
                    #FIXME: here we should send mail to the patch author;
                    continue
                if len(self.info) < 1:
                    #find tests to run
                    self.info = info
                    result = self.find_tests_to_run()
                    if not result:
                        return False

                # build wine :
                build = repo.force_build_wine()
                if not build:
                    self.error("Can't build wine. The current version is broken!")
                    # try to apply the patch
                    result = repo.apply_commit(self.filename)
                    if not result:
                        self.fixme("Can't apply the commit!!! This patch can't help. Aborting this patch");
                        continue
                    # build wine again:
                    build = repo.force_build_wine()
                    if not build:
                        self.error("Can't build wine with patch. The current version is broken!")
                        continue
#                    self.error("Cant't build wine. The current version is broken!")
#                    continue

                # run the tests
                if len(self.tests) < 1:
                    self.report("Nothing to test. Skip");
                else:
                    results_before = repo.run_tests(self.tests, ".before")

                # try to apply the patch
                result = repo.apply_commit(self.filename)
                if not result:
                    self.fixme("Cant'apply the commit!!! Aborting this patch");
                    #return False
                    continue

                # build wine again
                build = repo.build_wine()
                if not build:
                    self.error("Build failed. The patch must be incorrect")
                    repo.revert_last_commit()
                    # FIXME: we should report to the patch author about this accident
                    continue

                # run the tests again
                if len(self.tests) < 1:
                    self.debug("Nothing to test. The patch is good for this repository");
                    applied_to +=1
                    continue
                results_after = repo.run_tests(self.tests, ".after")

                failed = self.is_failed_tests(results_before, results_after)
                if failed:
                    self.error("There are failed tests. Probably the patch is incorrect")
                    repo.revert_last_commit()
                    continue

                applied_to += 1
        except:
            self.error("exception occured:" + traceback.format_exc() )

        self.debug("Can be applied to " + str(applied_to) + " repositories from " + str(len(self.self_repos)))

        # we should first run tests, and do not publish any repository if any test failed
        if applied_to != len(self.self_repos):
            # revert all changes
            for repo in self.self_repos:
                repo.remove_tmp_branch()
                repo.unset_mail_report()
            self.report_rejected()
            return False

        published_to = 0
        self.debug("The patch is good enough to publish it")
        for repo in self.self_repos:
            repo.merge_tmp_branch()
            if not self.testing_mode:
                result = repo.publish()
                if not result:
                    self.error("Can't publish your patch to the repository " + repo.name)
                    repo.revert_last_commit()
                else:
                    self.report("Your patch has been published to the repository " + repo.name)
                    published_to += 1
            else:
                self.report("Testing mode: Revert patch instead of publish")
                repo.revert_last_commit()
            repo.unset_mail_report()

        if published_to != len(self.self_repos):
            self.report_partially_applied()
            return True
        self.report("Your patch is applied");
        self.report_applied()

        return True

    def run(self):
        """ Run apply task """
        result = self.apply_with_tests()
        self.remove_patch_file()
        return result
