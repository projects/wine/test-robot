#!/usr/bin/python

# File: wine_repository.py
#
# This file is part of the Wine@TestRobot project
#
# Copyright (c) 2010-2011 Vitaly Perov

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import os
import re
from time import *
from git_repository import *
from helper import *

class wine_repository(git_repository):
    BUILD_COMMAND = "jmake"
    CLEAN_COMMAND = "make clean"
    FORCE_BUILD_COMMAND = CLEAN_COMMAND + " && ./configure && " + BUILD_COMMAND
    #FORCE_BUILD_COMMAND = "make clean && ./configure && " + BUILD_COMMAND
    EXPORT_DISPLAY = "export DISPLAY=:99"
    XVFB_DISPLAY = "Xvfb :99"
    XVFB_RUN_COMMAND = EXPORT_DISPLAY + " && " + XVFB_DISPLAY + " && xvfb-run --auto-servernum --server-num=1 "

    BUILD_LOG_FILE = "wine_build.log"
    TEST_GREP = " 2>&1 | cut -c1-60 | grep 'Test failed'"

    def build_wine(self):
        self.debug("Run wine-build script")
        output_file = os.path.join(self.git.working_dir, self.BUILD_LOG_FILE)
        result = run_with_log(self.git.working_dir, self.BUILD_COMMAND, output_file)
        if result:
            self.debug("Wine build complete")
            return True;
        self.error("Wine build failed. Logfile attached.")
        self._attach_log_(output_file)
        return False

    def force_build_wine(self):
        self.debug("Run wine-build script")
        output_file = os.path.join(self.git.working_dir, self.BUILD_LOG_FILE)
        result = run_with_log(self.git.working_dir, self.BUILD_COMMAND, output_file)
        if result:
            self.debug("Wine build is complete")
            return True;
        self.error("Wine build failed. Try to run ./configure first")
        result = run_with_log(self.git.working_dir, self.FORCE_BUILD_COMMAND, output_file)
        if result:
            self.debug("Wine force build is complete")
            return True;
        self.error("Wine force build failed.")
        self._attach_log_(output_file)
        return False

    def run_with_wine(self, command, output):
        wine = os.path.join(self.git.working_dir, "wine")
        command = wine + " " + command
        result = run_with_log("", command, output)
        self.debug("Run command '" + command + "'. Result=" + str(result))
        return result

    def run_test(self, test, postfix):
        """ runs the given test and creates the file with result"""
        # FIXME: different dlls can have the same test names. If we store
        #       all results in the same directory, the results will be overwritten

        base_path = self.git.working_dir
        executable = os.path.join(base_path, test['executable'])
        if not os.path.exists(executable):
            self.error("the path '" + executable + "' doesn't exists. Maybe this dll have no tests?")
            return ""
        resultfile = test['test_name'] + ".tst" + postfix
        resultfile = os.path.join(TMP_DIR, resultfile)
        command = self.XVFB_RUN_COMMAND + executable + " " + test['test_name'] + self.TEST_GREP

        self.debug("Executing the test (" + command + ")");
        result = self.run_with_wine(command, resultfile)
        if not result:
            self.error("test execution failed");
            return ""
        if not os.path.exists(resultfile):
            self.fixme("the result file '" + resultfile + "' doesn't exists.")
            return "";
        return resultfile

    def run_tests(self, tests, postfix):
        if len(tests) < 1:
            self.error("tests list is empty")
            return []
        results = []
        for test in tests:
            result = self.run_test(test, postfix)
            results.append(result)
        return results

    def merge_and_test(self, commit_id):
        """ Merge repository with the given commit, and try to build Wine then.
            If no commit_id is specified, repository is to be merged with a
            HEAD of an upstream"""
        result = self.build_wine()
        if not result:
            self.error("Cant't build wine. The current version is broken!")
            # FIXME: report by mail
            return False
        # Current version is good
        prev_state = self.head.reference
        if commit_id:
            diff = self.merge_with_commit(commit_id)
        else:
            diff = self.merge_with_upstream()
        if diff:
            self.error("Conflict occured during the merge")
            # FIXME: we should try to resolve simple conflicts
            self.report("Conflicts:\n" + diff + "\n");
            #stats = Stats._list_from_string(self, diff)
            result = self.resolve_conflicts(diff)
            if not result:
                self.error("Can't resolve the conflicts automatically. Reset to a previous state")
                self.head.reference = prev_state
                self.head.reset(index=True, working_tree=True)
                return False
            self.report("Conflicts: are resolved successfully");
        # Merged successfully. Build again
        result = self.build_wine()
        if not result:
            self.error("Cant't build wine after merge. Try to merge manually and fix error then")
            self.head.reference = prev_state
            self.head.reset(index=True, working_tree=True)
            # FIXME: report by mail
            return False
        result = self.publish()
        if not result:
            self.head.reference = prev_state
            self.error("Can't publish the repository " + self.name + " after merge.")
            return False
        self.debug("Merged successfully. Repository published successfully.")
        return True

    def resolve_conflict_in(self, filename):
        """ Resolve conflicts in particular files """
        # first try to resolve wine-independend conflicts:
        if git_repository.resolve_conflict_in(self, filename):
            return True

        # try to resolve Wine-dependend conflicts
        if filename == "VERSION":
            self.debug("Auto-resolving conflict in 'VERSION'")
            os.chdir(self.git.working_dir)
            f = open("VERSION", "r")
            lines = f.read()
            lines = lines.split('\n')
            f.close()
            version = ""
            for line in lines:
                if line.count("Wine version") > 0:
                    row = line.split(' ')
                    version = row[2].replace('-', '.')
                    break
            self.debug("New version is " + version)
            if len(version) < 1:
                self.error("Can't determine current Wine version")
                return False
            version_str = "WINE@Etersoft version " + version + "-eter1\n"
            f = open("VERSION", "w")
            f.write(version_str)
            f.close()
            # Update spec-file:
            if not self.specfile:
                self.error("Specfile for this repository is not set")
                return False
            spec_path = os.path.join(self.git.working_dir, self.specfile)
            f = open(spec_path, "r")
            spec = f.read()
            f.close()
            spec = re.sub(r'Version:\s\S*',"Version: " + version ,spec)
            spec = re.sub(r'Release:\s\S*',"Release: alt1", spec)
            changelog = "* " + strftime("%a %b %d %Y", gmtime()) + " " + MAIL_FROM  + " " + version + "-alt1\n" + "- new release " + version + "\n"
            spec = re.sub(r'%changelog',"%changelog\n" + changelog, spec)
            f = open(spec_path, "w")
            f.write(spec)
            f.close()
            # NOTE: conflict in 'configure' file will be resolved later, in next filename iteration
            return True
        return False
